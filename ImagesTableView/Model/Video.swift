//
//  Video.swift
//  ImagesTableView
//
//  Created by mbtec22 on 4/30/21.
//  Copyright © 2021 Tecsup. All rights reserved.
//

import Foundation
import UIKit

class Video {
    var image: UIImage
    var title: String
    
    init(image: UIImage, title: String) {
        self.image = image
        self.title = title
    }
}

//
//  VideoListScreen.swift
//  ImagesTableView
//
//  Created by mbtec22 on 4/29/21.
//  Copyright © 2021 Tecsup. All rights reserved.
//

import UIKit

class VideoListScreen: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var videos: [Video] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        videos = createArray()

    }
    func createArray() -> [Video] {
        var tempVideos: [Video] = []
        
        let video1 = Video(image: UIImage(named: "imagen")!, title: "primera app")
        let video2 = Video(image: UIImage(named: "imagen")!, title: "video 2")
        let video3 = Video(image: UIImage(named: "imagen")!, title: "video 3")
        let video4 = Video(image: UIImage(named: "imagen")!, title: "video 4")
        let video5 = Video(image: UIImage(named: "imagen")!, title: "video 5")
        let video6 = Video(image: UIImage(named: "imagen")!, title: "video 6")
        
        tempVideos.append(video1)
        tempVideos.append(video2)
        tempVideos.append(video3)
        tempVideos.append(video4)
        tempVideos.append(video5)
        tempVideos.append(video6)
        
        return tempVideos
    }
    
}

extension VideoListScreen: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return videos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let video = videos[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "VideoCell") as! VideoCell
        
        cell.setVideo(video: video)
        
        return cell
        
    }
}
